const withPlugins = require('next-compose-plugins');
const optimizedImages = require('next-optimized-images');

const nextConfiguration = {
  distDir: 'build',
  output: 'export',
  images: {
    disableStaticImages: true
  },
  compress: true,
};

module.exports = withPlugins([optimizedImages], nextConfiguration);
