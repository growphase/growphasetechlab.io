import { useState } from "react";
import { Box, Text } from "theme-ui";
import { IoIosClose } from "react-icons/io";
import { IoArrowForwardCircle, IoArrowBackCircle } from "react-icons/io5";

const PortfolioItemModal = ({ isOpen, onClose, projectDetails }) => {
    const [currentIndex, setCurrentIndex] = useState(0);
    const images = projectDetails?.galleryImages || [];

    const goToNextSlide = () => {
        setCurrentIndex((prevIndex) =>
            prevIndex === images.length - 1 ? 0 : prevIndex + 1
        );
    };

    const goToPrevSlide = () => {
        setCurrentIndex((prevIndex) =>
            prevIndex === 0 ? images.length - 1 : prevIndex - 1
        );
    };

    if (!isOpen) return <></>;

    return (
        <Box sx={styles.modalContainer}>
            <Box sx={styles.modalBackdrop} onClick={onClose}></Box>
            <Box sx={styles.modalContent}>
                <Box sx={styles.closeIcon}>
                    <IoIosClose size="32px" onClick={onClose} />
                </Box>

                <Box
                    sx={{
                        justifyContent: "space-between",
                        alignItems: "center",
                    }}
                >
                    <h3>{projectDetails.title}</h3>
                </Box>
                <Text>{projectDetails.description}</Text>

                {images.length > 0 &&
                    <Box sx={styles.imageCarousel}>
                        <IoArrowBackCircle
                            size="32px"
                            onClick={goToPrevSlide}
                            style={{ ...styles.imageCarousel.leftButton, ...disableSelectionStyle}}
                        />
                        <img
                            src={images[currentIndex]}
                            alt={`Slide ${currentIndex + 1}`}
                        />
                        <IoArrowForwardCircle
                            size="32px"
                            onClick={goToNextSlide}
                            style={{ ...styles.imageCarousel.rightButton, ...disableSelectionStyle}}
                        />
                    </Box>
                }
            </Box>
        </Box>
    );
};

export default PortfolioItemModal;

const styles = {
    modalContainer: {
        position: "fixed",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        zIndex: 9999,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    modalBackdrop: {
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        width: "100%",
        height: "100%",
        position: "absolute",
    },
    modalContent: {
        backgroundColor: "white",
        p: "24px",
        borderRadius: 4,
        width: "100%",
        maxWidth: 800,
        minHeight: 500,
        zIndex: 1,
        position: "relative",
    },
    closeIcon: {
        position: "absolute",
        right: "6px",
        top: "6px",
        cursor: "pointer",
    },
    imageCarousel: {
        height: [200, null, 400, null, null, null, null, null],
        display: "flex",
        position: "relative",
        margin: ["16px 0", null, "32px 0", null, null, null, null, null],

        img: {
            maxWidth: 600,
            width: "100%",
            height: "100%",
            objectFit: "contain",
            margin: "auto",
        },

        leftButton: {
            position: "absolute",
            top: "50%",
            left: 0,
            cursor: "pointer",
        },
        rightButton: {
            position: "absolute",
            top: "50%",
            right: 0,
            cursor: "pointer",
        },
    },
};

const disableSelectionStyle = {
    WebkitTouchCallout: 'none', /* iOS Safari */
    WebkitUserSelect: 'none', /* Safari */
    KhtmlUserSelect: 'none', /* Konqueror HTML */
    MozUserSelect: 'none', /* Firefox */
    msUserSelect: 'none', /* Internet Explorer/Edge */
    userSelect: 'none', /* Non-prefixed version, currently supported by Chrome and Opera */
};