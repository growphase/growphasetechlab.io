import facebook from 'assets/images/icons/facebook.png';
import twitter from 'assets/images/icons/twitter.png';
import github from 'assets/images/icons/github.png';
import dribbble from 'assets/images/icons/dribbble.png';
import linkedin from 'assets/images/icons/linkedin.png';
export const menuItems = [
  {
    id: 2,
    title: 'About Us',
    items: [
      {
        path: '#home',
        label: 'About Us',
      },
      {
        path: '#services',
        label: 'Services',
      },
      {
        path: '#team',
        label: 'Team',
      },,
      {
        path: '#other-services',
        label: 'Other Services',
      },
    ],
  },
  {
    id: 5,
    title: 'Connect',
    items: [
      {
        path: 'https://www.facebook.com/growphasetech',
        icon: facebook,
        label: 'Facebook',
      },
      {
        path: 'https://gitlab.com/growphase',
        icon: github,
        label: 'Gitlab',
      },
      {
        path: 'https://www.linkedin.com/company/growphase-technologies',
        icon: linkedin,
        label: 'LinkedIn',
      },
    ],
  },
];
