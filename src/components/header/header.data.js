export default [
    {
        path: 'home',
        label: 'Home',
    },
    {
        path: 'services',
        label: 'Services',
    },
    {
        path: 'team',
        label: 'Team',
    },
    {
        path: 'other-services',
        label: 'Other Services',
    },
    {
        path: 'portfolio',
        label: 'Portfolio',
    },
    {
        path: 'subscribe-us',
        label: 'Subscribe Us',
    }
];
