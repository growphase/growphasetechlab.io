/** @jsxRuntime classic */
/** @jsx jsx */
import {Box, Container, jsx} from 'theme-ui';
import SectionHeading from 'components/section-heading';
import Service from 'components/cards/service';
import icon3 from 'assets/images/icons/service3.png';
import icon4 from 'assets/images/icons/service4.png';
import icon5 from 'assets/images/icons/service5.png';
import icon6 from 'assets/images/icons/service6.png';
import icon7 from 'assets/images/icons/service7.png';
import icon8 from 'assets/images/icons/service8.png';

const data = [

    {
        id: 1,
        icon: icon4,
        title: 'Mobile & Web Development',
        description: `We build customised mobile and web apps for clients in various sectors including finance, education and local businesses. We offer end to end digital solutions to deliver frictionless user experiences.`,
    },
    {
        id: 5,
        icon: icon8,
        title: 'Smart AI Solutions',
        description: `We provide AI and ML development services to help businesses leverage the power of AI and ML technology. Our  services include providing deeper insights to your data and provide predictive analytics to help you make better decisions.`,
    },
    {
        id: 4,
        icon: icon7,
        title: 'Blockchain Development',
        description: `We provide blockchain development services to help businesses leverage the power of blockchain technology. Our blockchain development services include custom dapps development, smart contract development, and more.`,
    }, {
        id: 0,
        icon: icon3,
        title: 'Product Life Cycle Management',
        description: `Streamline product life cycles with our comprehensive project management from inception to end-of-life, we maximize efficiency, innovation, and collaboration.`,
    },
    {
        id: 2,
        icon: icon5,
        title: 'Fintech Solutions',
        description: `Get your info tests delivered at home collect a sample from the your progress tests.`,
    },
    {
        id: 3,
        icon: icon6,
        title: 'E-commerce Solutions',
        description: `Revolutionizing online retail with our Ecommerce solutions. Streamlined interfaces, seamless payments, exponential growth."`,
    }
];

const OtherServices = () => {
    return (
        <Box as="section"  id="other-services" sx={styles.section}>
            <Container sx={styles.container}>
                <SectionHeading
                    sx={styles.heading}
                    title="Other services"
                    description="We provide a wide range of services to help businesses leverage the power of technology."
                />
                <Box sx={styles.contentWrapper}>
                    {data?.map((item) => (
                        <Service key={item.id} item={item}/>
                    ))}
                </Box>
            </Container>
        </Box>
    );
};
export default OtherServices;

const styles = {
    section: {
        backgroundColor: '#F9FAFC',
        pt: [9, 9, 9, 11],
        pb: [9, 9, 9, 12, 12, 14],
    },
    container: {
        maxWidth: ['100%', null, null, null, '970px', '1140px', '1360px'],
    },
    heading: {
        mb: [6, null, null, 8, 9, null, 13],
        p: {
            maxWidth: 500,
            margin: '10px auto 0',
        },
    },
    contentWrapper: {
        gap: ['30px 30px', '30px 30px', '30px 30px', '80px 30px'],
        display: 'grid',
        justifyContent: ['center', 'center', 'center', 'unset'],
        gridTemplateColumns: [
            'repeat(1, 285px)',
            'repeat(1, 325px)',
            'repeat(1, 285px)',
            'repeat(3, 1fr)',
        ],
    },
};
