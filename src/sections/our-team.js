/** @jsxRuntime classic */
/** @jsx jsx */
import { useRef, useState, useEffect } from 'react';
import { jsx, Box, Container, Image } from 'theme-ui';
import SwiperCore, { Navigation, Pagination } from 'swiper';
import SectionHeading from 'components/section-heading';
import TeamMember from 'components/cards/team-member';

import avatar1 from 'assets/images/team/sagar.jpeg';
import avatar2 from 'assets/images/team/prabesh.jpeg';
import arrowRight from 'assets/images/icons/arrow-right.png';

SwiperCore.use([Navigation, Pagination]);

const data = [
  {
    id: 1,
    avatar: avatar1,
    name: 'Sagar Duwal',
    designation: 'Co-founder & CEO',
    socialLinks: [
      {
        name: 'twitter',
        link: 'https://twitter.com/ultimateg33k',
      },
      {
        name: 'github',
        link: 'http://github.com/sagarduwal',
      },
      {
        name: 'linkedin',
        link: 'https://www.linkedin.com/in/sagar-duwal-4771792a/',
      },
    ],
  },
  {
    id: 2,
    avatar: avatar2,
    name: 'Prabesh Suwal',
    designation: 'Co-founder & COO',
    socialLinks: [
      {
        name: 'twitter',
        link: 'http://twitter.com',
      },
      {
        name: 'linkedin',
        link: 'https://www.linkedin.com/in/prabesh-suwal/',
      },
    ],
  }
];

const OurTeam = () => {
  const swiperRef = useRef(null);
  const containerRef = useRef(null);
  const [currentIndex, setCurrentIndex] = useState(0);
  // const [containerOffset, setContainerOffset] = useState({
  //   left: null,
  //   top: null,
  // });

  const isEnd = swiperRef?.current?.swiper?.isEnd;

  const handlePrev = () => {
    swiperRef?.current?.swiper?.slidePrev();
    setInterval(() => {
      setCurrentIndex(swiperRef?.current?.swiper?.activeIndex);
    }, 100);

    clearInterval();
  };
  const handleNext = () => {
    swiperRef?.current?.swiper?.slideNext();
    setInterval(() => {
      setCurrentIndex(swiperRef?.current?.swiper?.activeIndex);
    }, 100);

    clearInterval();
  };

  // useEffect(() => {
  //   setContainerOffset({
  //     left: containerRef.current.offsetLeft,
  //     top: containerRef.current.offsetTop,
  //   });
  // }, [containerRef]);

  const breakpoints = {
    0: {
      slidesPerView: 1,
      spaceBetween: 0,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 30,
    },
    1024: {
      slidesPerView: 4,
      spaceBetween: 30,
    },
    1601: {
      slidesPerView: 5,
      spaceBetween: 30,
    },
  };

  return (
    <Box as="section" id="team" sx={styles.section}>
      <Container ref={containerRef}>
        <SectionHeading
          sx={styles.heading}
          title="Work with our team"
          description="Build an incredible workplace and grow your business with us."
        />
      </Container>
      <Box
        sx={styles.teamMembers}
        // style={{ marginLeft: currentIndex === 0 ? `${containerOffset?.left}px` : 0 }}
      >
        {currentIndex !== 0 && (
          <button
            onClick={handlePrev}
            className="swiper-arrow swiper-arrow-left"
          >
            <Image src={arrowRight} alt="arrow left" />
          </button>
        )}

        {data?.map((item, index) => (
          <div key={index}>
            <TeamMember member={item} />
          </div>
        ))}

      </Box>
    </Box>
  );
};

export default OurTeam;

const styles = {
  section: {
    pt: [11],
    pb: [11, null, null, 12, null, 14],
  },
  heading: {
    p: {
      maxWidth: 500,
      m: '10px auto 0',
    },
  },
  teamWrapper: {
    position: 'relative',
    pl: [2],
    pr: [2, null, null, 0],
    transition: '0.3s ease-in-out 0s',
  },
  teamMembers: {
    display: 'flex',
    justifyContent: 'center',
    gap: 32,
    flexWrap: 'wrap',
  }
};
