/** @jsxRuntime classic */
/** @jsx jsx */
import { useState } from "react";
import { Box, Container, jsx } from "theme-ui";
import SectionHeading from "components/section-heading";
import PortfolioItemModal from "components/PortfolioItemModal/PortfolioItemModal";

const data = [
    {
        id: 5,
        bg: "#3056b9",
        title: "Branchless Banking System",
        description: `Branchless banking system is a system that allows customers to perform basic banking transactions without the need to visit a bank branch. It is also known as a virtual bank.`,
        category: "Blockchain EVM",
        galleryImages: [
            "https://images.pexels.com/photos/20059053/pexels-photo-20059053/free-photo-of-a-snowy-forest-with-tall-trees-and-snow.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
            "https://images.pexels.com/photos/20035400/pexels-photo-20035400/free-photo-of-a-view-of-a-city-from-a-window.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        ],
    },
    {
        id: 4,
        bg: "#dd9131",
        title: "ERC20 and ERC721/ERC1155 Payout Claim Solution",
        description: `Blockchain System developed for any Web3 based company to distribute their tokens to their users. It is a complete solution for any company to distribute their tokens to their users. Admin could upload the claim rewards and enable users to claim the tokens. The system includes Solidity Smart Contract, backend to handle merkle tree generation and frontend to claim the tokens.`,
        category: "Blockchain EVM",
        galleryImages: [],
    },
    {
        id: 4,
        bg: "#dd9131",
        title: "Contract Event Pusher",
        description: `A backend service that provides a simple and easy-to-use interface for managing and monitoring smart contracts on any EVM-compatible network. The backend service is built on top of a highly scalable and reliable infrastructure, ensuring that it can handle even the most demanding workloads. It is also fully secure, with all data encrypted at rest and in transit.`,
        category: "Blockchain EVM",
        galleryImages: [],
    },
    {
        id: 0,
        bg: "#9dc840",
        title: "Lead Score ML Model",
        description: `A machine learning model that predicts the likelihood of a lead converting into a customer. The model uses historical data to identify patterns and trends that are indicative of a lead's likelihood to convert. This information can then be used to prioritize leads and focus sales and marketing efforts on those that are most likely to convert.`,
        category: "",
        galleryImages: [],
    },
    {
        id: 0,
        bg: "#9dc840",
        title: "Centralize Document Filing System",
        description: `A centralized document filing system that allows users to store, organize, and access documents from a single location. The system provides a secure and efficient way to manage documents, with features such as version control, access control, and search functionality. It is designed to streamline document management processes and improve collaboration among team members.`,
        category: "",
        galleryImages: [],
    },
    {
        id: 1,
        bg: "#9dc840",
        title: "Warranty Contract Management",
        description: ``,
        category: "Web Development",
        galleryImages: [],
    },
];

const Portfolio = () => {
    const [selectedProjectDetails, setSelectedProjectDetails] = useState();
    const [showProjectModal, setShowProjectModal] = useState(false);

    const handleProjectClick = (projectDetails) => {
        console.log(projectDetails);
        setSelectedProjectDetails(projectDetails);
        setShowProjectModal(true);
    };

    return (
        <Box as="section" id="portfolio" sx={styles.section}>
            <Container sx={styles.container}>
                <SectionHeading
                    sx={styles.heading}
                    title="Portfolio"
                    description=""
                />
                <Box sx={styles.contentWrapper}>
                    {data?.map((item, index) => (
                        <Box
                            sx={styles.projectItem}
                            style={{ background: item.bg }}
                            key={index}
                            onClick={() => handleProjectClick(item)}
                        >
                            <h3>{item.title}</h3>

                            <p>{item.description}</p>
                        </Box>
                    ))}
                </Box>
            </Container>

            <PortfolioItemModal
                isOpen={showProjectModal}
                onClose={() => setShowProjectModal(false)}
                projectDetails={selectedProjectDetails}
            />
        </Box>
    );
};
export default Portfolio;

const styles = {
    section: {
        backgroundColor: "#F9FAFC",
        pt: [9, 9, 9, 11],
        pb: [9, 9, 9, 12, 12, 14],
    },
    container: {
        maxWidth: ["100%", null, null, null, "970px", "1140px", "1360px"],
    },
    heading: {
        mb: [6, null, null, 8, 9, null, 13],
        p: {
            maxWidth: 500,
            margin: "10px auto 0",
        },
    },
    contentWrapper: {
        gap: ["30px 30px", "30px 30px", "30px 30px", "80px 30px"],
        display: "grid",
        justifyContent: ["center", "center", "center", "unset"],
        gridTemplateColumns: [
            "repeat(1, 285px)",
            "repeat(2, 325px)",
            "repeat(2, 285px)",
            "repeat(3, 1fr)",
        ],
    },
    projectItem: {
        width: "100%",
        maxWidth: 400,
        minHeight: 400,
        color: "#FFFFFF",
        padding: 24,
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        cursor: "pointer",

        h3: {
            fontSize: 24,
        },
    },
};
