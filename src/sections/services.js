/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, Box, Container } from 'theme-ui';
import { rgba } from 'polished';
import SectionHeading from 'components/section-heading';
import Service from 'components/cards/service';
import icon1 from 'assets/images/icons/service1.png';
import icon2 from 'assets/images/icons/service2.png';
import icon3 from 'assets/images/icons/service3.png';

const data = [
  {
    id: 1,
    icon: icon1,
    title: 'Artificial Intelligence AI/ML/LLM',
    description: `Unlock business potential with our tailored AI/ML/LLM solutions. Transform operations and make informed decisions with intelligent systems designed just for you.`,
  },
  {
    id: 3,
    icon: icon2,
    title: 'Blockchain',
    description: `Decentralized apps, secure smart contracts, and unrivaled digital innovation. We’re the blockchain development company that delivers.`,
  },
  {
    id: 4,
    icon: icon3,
    title: 'Mobile/Web Application',
    description: `We build mobile and web apps that people love to use. We’re the app development company that delivers frictionless user experiences.`,
  },
];

const Services = () => {
  return (
    <Box as="section" id="services" sx={styles.section}>
      <Container sx={styles.container}>
        <SectionHeading
          sx={styles.heading}
          title="Grow your startup with our Service"
          description="Build an incredible workplace and grow your business with us."
        />
        <Box sx={styles.contentWrapper}>
          {data?.map((item) => (
            <Service key={item.id} item={item} />
          ))}
        </Box>
      </Container>
    </Box>
  );
};

export default Services;

const styles = {
  section: {
    backgroundColor: rgba('#FFF5ED', 0.5),
    pt: [11, 11, 11, 12, 12, 12, 14],
    pb: [7, 7, 7, 9, 9, 10, 11],
  },
  container: {
    maxWidth: ['100%', null, null, null, '970px', '1140px', '1360px'],
  },
  heading: {
    maxWidth: [null, null, null, 455, 660],
    mb: [6, null, null, 8, null, 9, 13],
  },
  contentWrapper: {
    gap: 30,
    display: 'grid',
    justifyContent: ['center', null, null, 'unset'],
    gridTemplateColumns: [
      'repeat(1, 285px)',
      'repeat(1, 325px)',
      'repeat(1, 285px)',
      'repeat(3, 1fr)',
    ],
  },
};
